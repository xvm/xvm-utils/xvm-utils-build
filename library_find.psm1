# SPDX-License-Identifier: LGPL-3.0+
# Copyright (c) 2017-2022 XVM Team



function Find-Application([string] $Command, [Switch] $Required)
{
    $cmd = Get-Command -Name $Command -ErrorAction SilentlyContinue

    if($cmd)
    {
        return $cmd.Path
    }

    if($Required)
    {
        Write-Error -Message "$($Command) is required"
        exit 1
    }

    return $false
}


function Find-FDBuild([Switch] $Required)
{
    if(${env:XVMBUILD_FDBUILD_FILEPATH})
    {
        $path = Find-Application ${env:XVMBUILD_FDBUILD_FILEPATH} -Required:$false
    }

    if(!$path)
    {
        $path = Find-Application "fdbuild" -Required:$false
    }

    if(!$path)
    {
        Edit-Path -Path "${PSScriptRoot}/bin/msil/fdbuild/" -Prepend
        $path = Find-Application "fdbuild.exe" -Required:$Required
    }

    ${env:XVMBUILD_FDBUILD_FILEPATH} = $path
    return $path
}


function Find-Flex([Switch] $Required)
{
    $playerVersions = ("11.0", "11.1")
    $os = Get-OS
    $java = Find-Java -Required:$Required

    if($java -eq $null)
    {
        return $false
    }

    #flex_home
    if(${env:FLEX_HOME} -eq $null)
    {
        if($os -eq "windows")
        {
            ${env:FLEX_HOME}="${env:LOCALAPPDATA}/FlashDevelop/Apps/flexsdk/4.6.0"
        }
        else {
            ${env:FLEX_HOME}="/opt/apache-flex"
        }
    }

    if(!$(Test-Path ${env:FLEX_HOME}))
    {
        if($Required)
        {
            Write-Error "Apache Flex directory is not found"
            exit 1
        }
        return $false
    }

    Edit-Path -Path "${env:FLEX_HOME}/bin/" -Append

    #compc
    if($os -eq "windows")
    {
        ${env:XVMBUILD_COMPC_FILEPATH}=Join-Path "${env:FLEX_HOME}" "bin/compc.bat"
    }
    else
    {
        ${env:XVMBUILD_COMPC_FILEPATH}=Join-Path "${env:FLEX_HOME}" "bin/compc"
    }
    if(!$(Test-Path ${env:XVMBUILD_COMPC_FILEPATH}))
    {
        if($Required)
        {
            Write-Error "Apache Flex compc file is not found"
            exit 1
        }
        return $false
    }

    #playerglobal
    if(${env:PLAYERGLOBAL_HOME} -eq $null)
    {
        ${env:PLAYERGLOBAL_HOME} = Join-Path "${env:FLEX_HOME}" "frameworks/libs/player/"
    }

    foreach($playerVersion in $playerVersions)
    {
        $filepath = Join-Path "${env:PLAYERGLOBAL_HOME}" "${playerVersion}"
        $filepath = Join-Path "${filepath}" "playerglobal.swc"
        if(!(Test-Path $filepath))
        {
            New-Item -ItemType Directory -Path $(Split-Path -Parent $filepath) -ErrorAction SilentlyContinue

            Invoke-WebRequest -Uri "https://github.com/nexussays/playerglobal/raw/master/${playerVersion}/playerglobal.swc"  -OutFile $filepath
        }
    }

    return ${env:FLEX_HOME}
}


function Find-Git([Switch] $Required)
{
    return Find-Application "git" -Required:$Required
}


function Find-Java([Switch] $Required)
{
    return Find-Application "java" -Required:$Required
}


function Find-Mercurial([Switch] $Required)
{
    return Find-Application "hg" -Required:$Required
}


function Find-Mono([Switch] $Required)
{
    if(${env:XVMBUILD_MONO_FILEPATH})
    {
        if(Find-Application ${env:XVMBUILD_MONO_FILEPATH})
        {
            return $true
        }
    }

    $os = $(Get-OS)

    if($os -eq "windows")
    {
        return $false
    }

    $path = Find-Application "mono" -Required:$Required
    if(!$path)
    {
        return $false
    }

    ${env:XVMBUILD_MONO_FILEPATH} = $path

    return $path
}


function Find-Mtasc([Switch] $Required)
{
    return Find-Application "mtasc" -Required:$Required
}


function Find-Patch([Switch] $Required)
{
    $patchProgram =  Find-Application "patch" -Required:$false

    if(!$patchProgram)
    {
        $os = Get-OS
        if($os -eq "windows")
        {
            $patchDir="$PSScriptRoot/bin/${os}_i686/patch"
        }
        else
        {
            $patchDir="$PSScriptRoot/bin/${os}_${Get-Architecture}/patch"
        }

        Edit-Path -Path "${patchDir}" -Prepend

        $patchProgram = Find-Application "patch" -Required:$Required
    }
    return $patchProgram
}


function Find-Rabcdasm([Switch] $Required)
{
    $path =  Find-Application "rabcdasm" -Required:$false
    if(!$path)
    {
        $os = Get-OS
        if($os -eq "windows")
        {
            $rabcdasmDir="$PSScriptRoot/bin/${os}_i686/swf-disasm"
        }
        else
        {
            $rabcdasmDir="$PSScriptRoot/bin/${os}_$(Get-Architecture)/swf-disasm"
        }

        Edit-Path -Path "${rabcdasmDir}" -Prepend

        $path = Find-Application "rabcdasm" -Required:$Required
    }

    return $path
}


function Find-SentryCli([Switch] $Required)
{
    $sentryCliProgram =  Find-Application "sentry-cli" -Required:$false

    if(!$sentryCliProgram)
    {
        $os = Get-OS
        $arch = Get-Architecture
        $cliDir="$PSScriptRoot/bin/${os}_${arch}/sentry-cli"

        Edit-Path -Path "${cliDir}" -Prepend

        $sentryCliProgram = Find-Application "sentry-cli" -Required:$Required
    }

    return $sentryCliProgram
}


function Find-Unzip([Switch] $Required)
{
    return Find-Application "unzip" -Required:$Required
}


function Find-Zip([Switch] $Required)
{
    $zipProgram =  Find-Application "zip" -Required:$false

    if(!$zipProgram)
    {
        $os = Get-OS
        $arch = Get-Architecture
        if($os -eq "windows")
        {
            $zipDir="$PSScriptRoot/bin/${os}_i686/zip"
        }
        else
        {
            $zipDir="$PSScriptRoot/bin/${os}_${arch}/zip"
        }

        Edit-Path -Path "${zipDir}" -Prepend

        $zipProgram = Find-Application "zip" -Required:$Required
    }
    return $zipProgram
}

