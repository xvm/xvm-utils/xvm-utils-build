# SPDX-License-Identifier: LGPL-3.0+
# Copyright (c) 2017-2022 XVM Team

function Build-AS3Proj($Project)
{
    if(${env:XVMBUILD_FDBUILD_FILEPATH} -eq $null)
    {
        Find-FDBuild -Required
    }

    if($(Get-OS) -eq "windows")
    {
        Invoke-Expression -Command "${env:XVMBUILD_FDBUILD_FILEPATH} -notrace -compiler:'${env:FLEX_HOME}' -cp:'' '$Project'"
    }
    else
    {
        if(${env:XVMBUILD_MONO_FILEPATH} -eq $null)
        {
            Find-Mono -Required | Out-Null
        }
        Invoke-Expression -Command "${env:XVMBUILD_MONO_FILEPATH} `"${env:XVMBUILD_FDBUILD_FILEPATH}`" -notrace -compiler:`"${env:FLEX_HOME}`" -cp:`"`" `"$Project`""
    }

    if($LASTEXITCODE -ne 0)
    {
        exit 1
    }
}

